import { render, screen } from '@testing-library/react';
import Banner from './components/banner/banner.component';


test("Should have web" , () => {
  render(<Banner/>);
  const element = screen.getByText(/WEB/i);
  expect(element).toBeInTheDocument();
})
